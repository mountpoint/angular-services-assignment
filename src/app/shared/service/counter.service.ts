import { Injectable } from '@angular/core';

@Injectable()
export class CounterService {
  counts = {
    activeToInactive: 0,
    inactiveToActive: 0
  };

  countAction(type: string) {
    if (type === 'a-to-i') {
      ++this.counts.activeToInactive;
    } else {
      ++this.counts.inactiveToActive;
    }
  }
}
