import { Component, OnInit } from '@angular/core';
import { CounterService } from './shared/service/counter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  counts: { activeToInactive, inactiveToActive };

  constructor(private counterService: CounterService) { }

  ngOnInit() {
    this.counts = this.counterService.counts;
  }
}
